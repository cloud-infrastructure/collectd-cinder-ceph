[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/collectd-cinder-ceph/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/collectd-cinder-ceph/-/commits/master)

# collectd-cinder-ceph

A collectd plugin written in bash to gather ceph metrics from a cinder node

The main purpose of this plugin is to send values locally so we can trigger alarms on those if trash is getting full

It gathers the following information:
  - items in the pool
  - items in the trash

## Configuration

And example of configuration file can be found in `config/`. For instance:

```
LoadPlugin exec
<Plugin exec>
  Exec "cinder:nogroup" "/usr/sbin/collectd-cinder-ceph.sh"
</Plugin>
```
