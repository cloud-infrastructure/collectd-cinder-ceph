Name:           collectd-cinder-ceph
Version:        1.0.2
Release:        3%{?dist}
Summary:        Simple collectd exec plugin to retrieve metrics from ceph in a cinder node
License:        GPLv3
Group:          Development/Libraries
BuildArch:      noarch
Url:            https://gitlab.cern.ch/cloud-infrastructure/collectd-cinder-ceph

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd
Requires:       ceph-common
Requires:       openstack-cinder

Requires(post):         collectd
Requires(preun):        collectd
Requires(postun):       collectd

%{?systemd_requires}
BuildRequires: systemd

%description
Simple collectd exec plugin that retrieves metrics from ceph in a cinder node

%prep
%setup -q -n %{name}-%{version}

%build
%install
install -p -D -m 755 src/collectd-cinder-ceph.sh %{buildroot}%{_sbindir}/collectd-cinder-ceph.sh

%post
%systemd_post collectd.service

%preun
%systemd_preun collectd.service

%postun
%systemd_postun_with_restart collectd.service

%files
%attr(0755, root, root) %{_sbindir}/collectd-cinder-ceph.sh
%doc src/README.md

%changelog
* Tue Apr 16 2024 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> 1.0.2-3
- Build for RHEL and Alma 9

* Wed Jul 05 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 1.0.2-2
- Build for RHEL & ALMA

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.2-1
- Improvements after AZ change

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.1-1
- Initial rebuild for el8s

* Thu Dec 10 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-5
- Finalize transition to sbin by removing script from bin

* Wed Dec 09 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-4
- Change executable path to sbin

* Fri Aug 21 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-3
- Change plugin type in sensor

* Fri Aug 21 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-2
- Fix format for plugin in collectd 

* Tue Aug 04 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-1
- Initial release of the cinder-ceph plugin
